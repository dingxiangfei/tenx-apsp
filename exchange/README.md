# Exchange

Exchange rate computation application.

## Installation

Install `elixir` for your operating system.

To build this application, invoke `mix deps.get` and then `mix escript.build` at this project directory.

After building is done, the application `exchange` is available.

## Usage

There are two input rules implemented.

### Exchange rate update
The rule is:
>`<date> <exchange> <currency1> <currency2> <forward_rate> <backward_rate>`
`date` should use ISO8601 format; `forward_rate` is the conversion rate
from `currency1` to `currency2` at the exchange `exchange`,
and `backward_rate` is the conversion rate in the other way around.

### Exchange rate request
The rule is:
>`EXCHANGE_RATE_REQUEST <exchange1> <currency1> <exchange2> <currency2>`
This will query the current best exchange rate from `currency1` at `exchange1` to
`currency2` at `exchange2` and display the conversion pathway.

## Design decisions
First, there are all-pair shortest-paths(APSP) algorithms on dynamic graphs available.
However, after reading the literature, only the level path system technique by
Mikkel Thorup (SWAT2004, pg.384-395) is claimed to be able to cope with fully dynamic
graphs with negative weights, which is required for this application, with update time O(n^2.6)
and query time O(log n).
In this publication, the author did not give a clear direction or proof for the correctness
on graphs with negative weights with his modification introduced to Demestrescu and Italiano
(STOC2013, pg.159-166).
I am currently communicating with the author about this issue and try to get a correct algorithm.

Therefore, so far only Floyd-Warshall version of dynamic APSP is used, which incurs O(1) query time
and O(n^3) update time.

In this application, the algorithm is separated from the main command line interface so that it may be
made into a standalone system.
The call to the algorithm is done through messaging, so that it can be adapted into server-client
architecture easily.

The reason to use Elixir/Erlang is to leverage high availability of this runtime system to deal with high
volume of requests and updates.


