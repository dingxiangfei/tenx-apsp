defmodule RequestRecord do
  defstruct src_exchange: nil, src_cur: nil, dst_exchange: nil, dst_cur: nil
end

defmodule UpdateRecord do
  defstruct date: nil, exchange: nil, cur1: nil, cur2: nil, r1: nil, r2: nil
end

defmodule Exchange do
  import Combine.Parsers.Base
  import Combine.Parsers.Text

  @moduledoc """
  Command Line Interface
  """

  def main(args) do
    loop (spawn APSPServer, :start, []), {MapSet.new, MapSet.new }
  end

  defp digits_to_num(digits) do
    {n, _} = Integer.parse(Enum.join(digits))
    n
  end

  @doc """
  Date parser combinator
  """
  def date_parser do
    pipe([
      digit() |> times(4), # yyyy
      skip(char(?-)) |> digit() |> times(2), # '-'MM
      skip(char(?-)) |> digit() |> times(2), # '-'dd
      skip(char(?T)) |> digit() |> times(2), # 'T'hh
      skip(char(?:)) |> digit() |> times(2), # ':'mm
      skip(char(?:)) |> digit() |> times(2), # ':'ss
      either(char(?+), char(?-)), # '+'|'-'
      digit() |> times(2), # tzmm
      skip(char(?:)) |> digit() |> times(2) # ':'tzss
    ], fn
      [year, month, day, hour, minute, second, tzsign, tzminute, tzsecond] ->
        %{
          year: digits_to_num(year),
          month: digits_to_num(month),
          day: digits_to_num(day),
          hour: digits_to_num(hour),
          minute: digits_to_num(minute),
          second: digits_to_num(second),
          tzsign: tzsign,
          tzminute: digits_to_num(tzminute),
          tzsecond: digits_to_num(tzsecond)
        }
    end)
  end

  @doc """
  Update rule parser combinator
  """
  def update_parser do
    pipe([
      date_parser(),
      skip(spaces()) |> word(), # ' '+EXCHANGE
      skip(spaces()) |> word(), # ' '+CUR1
      skip(spaces()) |> word(), # ' '+CUR2
      skip(spaces()) |> float(), # ' '+R1
      skip(spaces()) |> float() # ' '+R2
    ], fn [date, exchange, cur1, cur2, r1, r2] ->
      %UpdateRecord{
        date: date,
        exchange: exchange,
        cur1: cur1,
        cur2: cur2,
        r1: r1,
        r2: r2
        } end)
  end

  @doc """
  Request rule parser combinator
  """
  def request_parser do
    pipe([
      skip(word_of(~r/EXCHANGE_RATE_REQUEST/)) |> skip(spaces()) |>
      word(), # src_exchange
      skip(spaces()) |> word(), #src_currency
      skip(spaces()) |> word(), #dst_exchange
      skip(spaces()) |> word() #dst_currency
    ], fn [src_exchange, src_cur, dst_exchange, dst_cur] ->
      %RequestRecord{
        src_exchange: src_exchange,
        src_cur: src_cur,
        dst_exchange: dst_exchange,
        dst_cur: dst_cur
        } end)
  end

  # input loop
  defp loop(apsp, {currencies, exchanges}) do
    input = String.trim(IO.gets "")
    case Combine.parse(input, either(update_parser, request_parser)) do
      [%RequestRecord{
        src_exchange: src_exchange,
        src_cur: src_cur,
        dst_exchange: dst_exchange,
        dst_cur: dst_cur,
      }] ->
        request_rate(apsp, src_exchange, src_cur, dst_exchange, dst_cur)

      [%UpdateRecord{
        date: _,
        exchange: exchange,
        cur1: cur1,
        cur2: cur2,
        r1: r1,
        r2: r2
      }] ->
        {currencies, exchanges} =
          update_exchange(apsp, {currencies, exchanges}, exchange, {cur1, r1}, {cur2, r2})
      {:error, msg} ->
        IO.puts "Input does not match a correct query rule. #{msg}"
    end
    loop apsp, {currencies, exchanges}
  end

  # request processor
  defp request_rate(apsp, src_exchange, src_cur, dst_exchange, dst_cur) do
    send apsp, {:get, self(), {src_exchange, src_cur}, {dst_exchange, dst_cur}}
    receive do
      {nil, nil} ->
        IO.puts "No conversion is possible"
      {weight, path} ->
        IO.puts "Best rate is #{:math.exp(-weight)}"
        IO.puts "Conversion pathway:"
        Enum.each path, fn {exchange, currency} -> IO.puts "Exchange: #{exchange}, Currency: #{currency}" end
    end
  end

  # update processor
  defp update_exchange(apsp, {currencies, exchanges}, exchange, {cur1, r1}, {cur2, r2}) do
    # if the exchange is new to the system, add in new edges
    if not MapSet.member? exchanges, exchange do
      Enum.each exchanges, fn ex ->
        send apsp, {:update, self(), {ex, cur1}, {exchange, cur1}, 0, 0}
        send apsp, {:update, self(), {ex, cur2}, {exchange, cur2}, 0, 0}
        Enum.each currencies, fn currency ->
          send apsp, {:update, self(), {ex, currency}, {exchange, currency}, 0, 0}
        end
      end
    end
    exchanges = MapSet.put exchanges, exchange

    # if the currency is new to the system, add in new edges
    Enum.each [cur1, cur2], fn currency ->
      if not MapSet.member? currencies, currency do
        Enum.each exchanges, fn ex1 ->
          Enum.each exchanges, fn ex2 ->
            if ex1 != ex2 do
              send apsp, {:update, self(), {ex1, currency}, {ex2, currency}, 0, 0}
            end
          end
        end
      end
    end
    currencies = currencies |> MapSet.put(cur1) |> MapSet.put(cur2)

    send apsp, {:update, self(), {exchange, cur1}, {exchange, cur2}, -:math.log(r1), -:math.log(r2)}
    send apsp, {:evaluate, self()}

    {currencies, exchanges}
  end
end
