defmodule APSPStruct do
  defstruct vertices: MapSet.new, distMatrix: %{}, edgeWeight: %{}
end

defmodule APSPServer do
  def start do
    loop %APSPStruct{}
  end
  defp loop(state) do
    receive do
      {:update, caller, u, v, w1, w2} ->
        loop(update state, u, v, w1, w2)
      {:evaluate, caller} ->
        loop(compute state)
      {:get, caller, u, v} ->
        send caller, get(state, u, v)
        loop state
    end
  end

  defp compute(state) do
    vertices = state.vertices
    edgeWeight = state.edgeWeight
    indices = for u <- vertices, v <- vertices, do: {u, v}

    # initialize distance matrix
    distMatrix = Map.new(Enum.map indices, fn {u, v} ->
      cond do
        u == v ->
          {{u, u}, {0, nil}}
        Map.has_key? edgeWeight, {u, v} ->
          {{u, v}, {edgeWeight[{u, v}], u}}
        true ->
          {{u, v}, nil}
      end
    end)

    # Floyd-Warshall
    distMatrix =
      Enum.reduce vertices, distMatrix, fn(u, distMatrix) ->
        Enum.reduce vertices, distMatrix, fn(v, distMatrix) ->
          Enum.reduce vertices, distMatrix, fn(x, distMatrix) ->
            if (distMatrix[{u, x}] != nil) and (distMatrix[{x, v}] != nil) do
              w = elem(distMatrix[{u, x}], 0) + elem(distMatrix[{x, v}], 0)
              if (distMatrix[{u, v}] == nil) or (w < elem(distMatrix[{u, v}], 0)) do
                distMatrix = Map.put distMatrix, {u, v}, {w, x}
              end
            end
            distMatrix
          end
        end
      end
    %APSPStruct{state | distMatrix: distMatrix}
  end

  defp update(state, u, v, w1, w2) do
    vertices = state.vertices |> MapSet.put(u) |> MapSet.put(v)
    edgeWeight = state.edgeWeight |> Map.put({u, v}, w1) |> Map.put({v, u}, w2)
    %APSPStruct{
      state |
      vertices: vertices,
      edgeWeight: edgeWeight
    }
  end

  defp fix(f) do
    (fn z -> z.(z) end).(fn x -> f.(fn y -> (x.(x)).(y) end) end)
  end

  defp get(state, u, v) do
    if Map.has_key? state.distMatrix, {u, v} do
      {w, _} = state.distMatrix[{u, v}]
      fix(fn loop -> fn {path, v} ->
          {_, prev} = state.distMatrix[{u, v}]
          if v == u do
            {w, [u | path]}
          else
            loop.({[v | path], prev})
          end
        end end).({[], v})
    else
      {nil, nil}
    end
  end

end
